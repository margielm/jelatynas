'use strict';

describe('Controller: RegisterCtrl should', function () {

    // load the controller's module
    beforeEach(module('jelatynaSApp'));

    var RegisterCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        RegisterCtrl = $controller('RegisterCtrl', {
            $scope: scope
        });
    }));

    it('add presentation to empty', function () {
        withSelected([]);

        scope.changed(true, {id: 1});

        expect(scope.participant.chosenPresentations).toEqual([
            {id: 1}
        ]);
    });

    it('add another presentation', function () {
        withSelected(asIds(1));

        scope.changed(true, {id: 2});

        expect(scope.participant.chosenPresentations).toEqual(asIds(1, 2));
    });


    it('remove presentation', function () {
        withSelected(asIds(1, 2));

        scope.changed(false, {id: 2});

        expect(scope.participant.chosenPresentations).toEqual(asIds(1));
    });

    it("be enabled if selected less than limit", function () {
        scope.limit = 2;
        withSelected([]);

        var disabled = scope.isDisabled({id: 2});

        expect(disabled).toBeFalsy();
    });

    it("be disabled if limit reached", function () {
        scope.limit = 1;
        withSelected([1]);

        var disabled = scope.isDisabled({id: 2});

        expect(disabled).toBeTruthy();
    });
    it("be enabled if limit reached but presentation already selected", function () {
        scope.limit = 1;
        withSelected(asIds(1));

        var disabled = scope.isDisabled({id: 1});

        expect(disabled).toBeFalsy();
    });

    it("return message if presentation disabled", function () {
        withSelected(asIds(1, 2, 3, 4, 5, 6, 7));

        var message = scope.getMessageFor({id: 8});

        expect(message).toBe("Możesz wybrać maksymalnie 7 prezentacji");
    });
    it("return empty message if presentation enabled", function () {
        scope.limit = 1;
        withSelected([]);

        var message = scope.getMessageFor({id: 1});

        expect(message).toBe("");
    });

    function withSelected(ids) {
        scope.participant.chosenPresentations = ids;


    }

    function asIds(/*ids*/) {
        return _.map(_.toArray(arguments), function (id) {
            return {id: id}
        });
    }

});
