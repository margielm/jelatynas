'use strict'

describe('Controller: AgendaCtrl Should', function () {

    beforeEach(module('jelatynaSApp'));

    var AgendaCtrl = {};
    var scope = {};

    beforeEach(inject(function ($controller, $rootScope, _Agenda_) {
        scope = $rootScope.$new();
        spyOn(_Agenda_, "get").and.returnValue({visibleRooms: []});
        AgendaCtrl = $controller('AgendaCtrl', {
            $scope: scope,
            Agenda: _Agenda_
        })
    }));


    function aPresentationInRoom(title, room) {
        return { room: room, presentation: {title: title} };
    }

    it('find all presentations for first slot', function () {
        var presentationInRoom = aPresentationInRoom("title 1", "A");
        withVisibleRooms("A");
        withSchedule(
            { slotId: 1, presentations: [ presentationInRoom ] },
            { slotId: 2, presentations: [ aPresentationInRoom("title 2", "A") ] }
        );

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentationInRoom]);
    });

    it('get presentation for second slot', function () {
        var presentationInRoom = aPresentationInRoom("title 2", "A");
        withVisibleRooms("A");
        withSchedule(
            { slotId: 1, presentations: [ aPresentationInRoom("title 1", "A") ] },
            { slotId: 2, presentations: [ presentationInRoom ] }
        );

        var presentations = scope.getAllPresentationsForSlot(2);

        expect(presentations).toEqual([presentationInRoom]);
    });


    it('get presentations from ALL rooms', function () {
        var presentation1 = aPresentationInRoom("title 1", "A");
        var presentation2 = aPresentationInRoom("title 2", "B");
        withVisibleRooms("A", "B");
        withSchedule(
            { slotId: 1, presentations: [presentation1, presentation2] }
        );

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentation1, presentation2]);
    })

    it('get presentations for visible rooms only', function () {
        var presentation = aPresentationInRoom("title 1", "A");
        withVisibleRooms("A");
        withSchedule(
            {   slotId: 1, presentations: [ presentation, aPresentationInRoom("title 2", "B") ] }
        );

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentation]);
    });


    describe("Rooms should", function () {

        it("be added to visible list", function () {
            withRooms(["A"]);

            scope.changed("A", true);

            expect(scope.agenda.visibleRooms).toEqual(["A"]);
        });


        it("be removed from visible list", function () {
            withRooms(["A"])
            scope.changed("A", true);

            scope.changed("A", false);

            expect(scope.visibleRooms).toEqual([]);
        });

        it("be sorted by given order", function () {
            var rooms = ["B", "C", "A"];
            withRooms(rooms);

            scope.changed("C", true);
            scope.changed("B", true);
            scope.changed("A", true);

            expect(scope.agenda.visibleRooms).toEqual(rooms);
        });

        it("be visible if on the list", function () {
            withVisibleRooms("A");

            var visible = scope.isVisible("A");

            expect(visible).toBeTruthy();
        });

        it("not be visible if not on the list", function () {
            withVisibleRooms("A");

            var visible = scope.isVisible("B");

            expect(visible).toBeFalsy();
        });

        it("be disabled if checking only selected room", function () {
            withVisibleRooms("A");

            var isDisable = scope.isDisabled("A");

            expect(isDisable).toBeTruthy();
        });

        it("not be disabled if checking different room", function () {
            withVisibleRooms("A");

            var isDisable = scope.isDisabled("B");

            expect(isDisable).toBeFalsy();
        });

        it("not be disabled if one of visible rooms", function () {
            withVisibleRooms("A", "B");

            var isDisable = scope.isDisabled("B");

            expect(isDisable).toBeFalsy();
        });
    });


    function withRooms(rooms) {
        scope.agenda.rooms = rooms;
    }

    function withSchedule(schedule) {
        scope.agenda.schedule = _.toArray(arguments);
    }

    function withVisibleRooms(rooms) {
        scope.agenda.visibleRooms = _.toArray(arguments);
    }

});