'use strict';

ddescribe('Filter: presentationFilter Should return', function () {

    beforeEach(module('jelatynaSApp'));

    var presentationFilter;

    beforeEach(inject(function ($filter) {
        presentationFilter = $filter('presentationFilter');
    }));

    it('true if filter string empty', function () {
        var presentation = aPresentation("abc");

        var isVisible = presentationFilter(presentation, "");

        expect(isVisible).toBeTruthy();
    });

    it("false if filter string not in presentation title nor speaker name", function () {
        var presentation = aPresentation("title", "name");

        var isVisible = presentationFilter(presentation, "b");

        expect(isVisible).toBeFalsy();
    });


    it("true if filter string in presentation title", function () {
        var presentation = aPresentation("abc", "XYZ");

        var isVisible = presentationFilter(presentation, "a");

        expect(isVisible).toBeTruthy();
    });

    it("true if filter string in presentation title case insensitive", function () {
        var presentation = aPresentation("Atom", "XYZ");

        var isVisible = presentationFilter(presentation, "aT");

        expect(isVisible).toBeTruthy();
    });

    it("true if filter string in speaker name case insensitive", function () {
        var presentation = aPresentation("Atom", "John Smith");

        var isVisible = presentationFilter(presentation, "HN s");

        expect(isVisible).toBeTruthy();
    });

    it("true if filter string in second speaker name", function () {
        var presentation = aPresentation("Atom", "John Smith", "Bob Robert");

        var isVisible = presentationFilter(presentation, "bob");

        expect(isVisible).toBeTruthy();
    });

    function aPresentation(title /*, speakers*/) {
        return {
            title: title,
            speakers: _.map(_.tail(_.toArray(arguments)), function (name) {
                return {fullName: name};
            })
        };
    }

});
