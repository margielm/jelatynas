'use strict';

xdescribe('Service: Vote', function () {

    beforeEach(module('jelatynaSApp'));

    var Vote, $http;
    beforeEach(inject(function (_Vote_, _$httpBackend_) {
        $http = _$httpBackend_;
        Vote = _Vote_;
    }));

    it('should do something', function () {
        $http.expectPOST("/v4p/abc", {key: "abc", vote: 1, presentationId: 2}).respond(200);

        Vote.save({key: "abc", vote: 1, presentationId: 2});

        $http.flush();
    });

});
