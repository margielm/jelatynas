'use strict';

angular.module('jelatynaSApp')
    .filter('presentationFilter', function () {

        function aFilter(filterString) {
            return {
                availableIn: function (inputString) {
                    return S(inputString.toLocaleLowerCase()).contains(filterString.toLowerCase());
                }
            }
        }

        return function (presentation, filterString) {
            var filter = aFilter(filterString);
            return filter.availableIn(presentation.title)
                || _.any(_.map(presentation.speakers, function (speaker) {
                    return filter.availableIn(speaker.fullName);
                }));
        };


    });
