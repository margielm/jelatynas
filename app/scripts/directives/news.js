'use strict';

angular.module('jelatynaSApp')
    .directive('news', function () {
        return {
            scope: {items: "="},
            templateUrl: '/views/directives/news.html',
            restrict: 'E'
        };
    });
