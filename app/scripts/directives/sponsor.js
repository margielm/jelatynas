'use strict';

angular.module('jelatynaSApp')
    .directive('sponsor', function () {
        return {
            scope: {items: '=', title: '@'},
            templateUrl: '/views/directives/sponsor.html',
            restrict: 'E'
        };
    });
