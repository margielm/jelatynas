'use strict';

angular.module('jelatynaSApp')
    .directive('menu', function () {
        return {
            templateUrl: '/views/directives/menu.html',
            restrict: 'E',
            replace: true,
            controller: function ($scope, $attrs) {
                $scope.classes = "nav nav-justified navbar-collapse collapse"
                if ($attrs['type'] === 'footer'){
                    $scope.classes = "nav navbar-nav navbar-right hidden-xs";
                }
                $scope.items = [
                    {name: "główna", url: "#/"},
                    {name: "Aktualności", url: "#/news"},
                    {name: "O nas", url: "#/about"},
                    {name: "Co, gdzie, kiedy?", url: "#/info"},
                    {name: "Agenda", url: "#/schedule"},
                    {name: "Prelegenci", url: "#/speakers"},
                    {name: "Prezentacje", url: "#/presentations"},
                    {name: "Partnerzy", url: "#/partners"},
                    {name: "Kontakt", url: "mailto:confitura@confitura.pl"}
                ];
            }
        };
    });
