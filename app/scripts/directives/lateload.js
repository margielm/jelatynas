'use strict';

angular.module('jelatynaSApp')
    .directive('lateLoad', function () {
        return {
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                scope.$on("late-load", function (event, name) {
                    if (name === attrs.id) {
                        window[attrs.id]();
                    }
                });
            }
        };
    });
