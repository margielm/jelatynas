'use strict';

angular.module('jelatynaSApp')
    .directive('people', function ($modal) {
        return {
            scope: {items: "="},
            templateUrl: '/views/directives/people.html',
            restrict: 'E'
        };
    });
