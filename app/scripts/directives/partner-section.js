'use strict';

angular.module('jelatynaSApp')
    .directive('partnerSection', function () {
        return {
            scope: {partners: "="},
            templateUrl: '/views/directives/partner-section.html',
            restrict: 'E'
        };
    });
