'use strict';

angular.module('jelatynaSApp')
    .controller('NewsCtrl', function ($scope, News) {
        var page = 0;
        var pageSize = 5;
        $scope.loading = false;

        $scope.news = [];

        $scope.nextPage = function () {
            $scope.loading = true;
            var loaded = News.query({page: page, pageSize: pageSize}, function () {
                page += 1;
                _.each(loaded, function (item) {
                    $scope.news.push(item);
                });
                $scope.loading = false;
            });
        };

        $scope.getLatest = function () {
            $scope.news = News.query({page: 0, pageSize: 1});
        }

    });
