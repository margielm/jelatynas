'use strict';

angular.module('jelatynaSApp')
    .controller('RegisterCtrl', function ($scope, Registration) {
        $scope.loading = true;
        $scope.participant = {mailing: false, chosenPresentations: []};
        $scope.setup = Registration.get(function () {
            $scope.loading = false;
        });
        $scope.limit = 7;
        $scope.disableMessage = "Możesz wybrać maksymalnie " + $scope.limit + " prezentacji";
        $scope.info = "Wskaż maksymalnie "+$scope.limit+" prezentacji które najbardziej Cię inseresują. Pomoże to nam ułozyć optymalną agende.";
        $scope.experienceList = [
            "Dopiero się uczę",
            "Zawodowo mniej niż rok",
            "Zawodowo 1-2 lata",
            "Zawodowo 2-4 lata",
            "Zawodowo 4-8 lata",
            "Zawodowo powyżej 8 lat"
        ];
        $scope.occupationList = [
            "Studentem/uczniem",
            "Programistą",
            "Testerem",
            "Kierownikiem zespołu",
            "Kierownikiem projektu",
            "Inne"
        ];
        $scope.sizes = ["S", "M", "L", "XL"];


        $scope.register = function (participant) {
            $scope.submitted = true;
            if ($scope.participantForm.$valid) {
                var registration = new Registration(participant);
                registration.$save()
                    .then(function () {
                        $scope.finish = true;
                    }).catch(function (response) {
                        $scope.internalError = response.data;
                    });
            }
        };

        $scope.changed = function (selected, presentation) {
            if (selected) {
                getSelected().push({id: presentation.id});
            } else {
                $scope.participant.chosenPresentations = _.reject(getSelected(), isA(presentation));
            }
        };

        $scope.isDisabled = function (presentation) {
            if (_.find(getSelected(), isA(presentation))) {
                return false;
            } else {
                return getSelected().length === $scope.limit;
            }
        };

        $scope.getMessageFor = function (presentation) {
            return $scope.isDisabled(presentation) ? $scope.disableMessage : "";
        };

        function getSelected() {
            return $scope.participant.chosenPresentations;
        }

        function isA(presentation1) {
            return function (presentation2) {
                return presentation1.id === presentation2.id
            }
        }

    });