'use strict';

angular.module('jelatynaSApp')
    .controller('RegisterConfirmationCtrl', function ($scope, Confirmation, $routeParams) {
        $scope.loading = true;
        $scope.success = true;

        $scope.confirm = function () {
            doConfirm("confirm");
        };

        $scope.final = function () {
            doConfirm("final");
        };

        $scope.cancel = function () {
            doConfirm("cancel");
        };

        $scope.getStatus = function () {
            Confirmation.get({"action": "status", "token": $routeParams.token})
                .$promise
                .then(function (result) {
                    console.log(result);
                    $scope.status = result.status;
                })
                .catch(function (response) {
                    $scope.status = "Wystąpił problem podczas pobierania statusu";
                });
        };

        function doConfirm(action) {
            var promise = Confirmation.save({"action": action, "token": $routeParams.token}).$promise;
            promise.catch(function (response) {
                $scope.error = response.data;
                $scope.success = false;
            });
            promise['finally'](function () {
                $scope.loading = false;
            });
        }

    });
