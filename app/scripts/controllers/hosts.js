'use strict';

angular.module('jelatynaSApp')
    .controller('HostsCtrl', function ($scope, Hosts, $modal) {
        var modal = {};
        $scope.hosts = Hosts.query({"type": "main"});
        $scope.volunteers = Hosts.query({"type": "volunteers"});
    });