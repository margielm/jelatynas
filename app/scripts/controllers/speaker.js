'use strict';

angular.module('jelatynaSApp')
    .controller('SpeakerCtrl', function ($scope, Speaker, $modal) {
        $scope.speakers = Speaker.query(function () {
            $scope.$broadcast('late-load', 'speakers');
        });



        $scope.show = function (speaker) {
            $scope.speaker = speaker;
            $modal.open({
                templateUrl: 'speaker-modal.html',
                windowClass: 'my-modal',
                scope: $scope
            });
        };
    });
