'use strict';

angular.module('jelatynaSApp')
    .controller('TwitterCtrl', function ($scope, Twitter) {

        Twitter.query().$promise.then(function (tweets) {
            console.log()
            var tweet = tweets[0];
            _.each(tweet.entities.hashtags, function (hashtag) {
                var name = hashtag.text;
                tweet.text = tweet.text.replace("#" + name, getUrlTo(name));

            });
            $scope.tweet = tweet;
        });

        $scope.toDate = function (stamp) {
            return moment(Date.parse(stamp)).fromNow();
        }

        function getUrlTo(name) {
            return "<a href='https://twitter.com/hashtag/:tag' target='_blank'>#:tag</a>".replace(/:tag/g, name);
        }

    });
