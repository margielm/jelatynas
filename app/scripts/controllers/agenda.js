'use strict';

angular.module('jelatynaSApp')
    .controller('AgendaCtrl', function ($scope, Agenda) {
        $scope.presentationFilter = '';
        $scope.visibleRooms = [];

        $scope.agenda = Agenda.get(function () {
            $scope.agenda.visibleRooms = $scope.agenda.rooms;

        });


        $scope.getAllPresentationsForSlot = function (slotId) {
            function roomIsVisible(presentationInRoom) {
                return $scope.agenda.visibleRooms.indexOf(presentationInRoom.room) !== -1;
            }


            return  _.chain(findSlotBy(slotId).presentations)
                .filter(function (presentationInRoom) {
                    return roomIsVisible(presentationInRoom) || $scope.availableForAllRooms(presentationInRoom);
                })
                .value();
        };

        $scope.changed = function (name, selected) {
            var agenda = $scope.agenda;
            if (selected) {
                agenda.visibleRooms.push(name);
                agenda.visibleRooms = _.sortBy(agenda.visibleRooms, function (room) {
                    return $scope.agenda.rooms.indexOf(room);
                });
            }
            else {
                agenda.visibleRooms = _.reject(agenda.visibleRooms, function (room) {
                    return room === name;
                });
            }
        };

        $scope.isVisible = function (room) {
            return _.contains($scope.agenda.visibleRooms, room);
        };

        $scope.getRoomSpanFor = function (presentation) {
            return presentation.room === 'ALL' ? $scope.agenda.visibleRooms.length : 1;
        };

        $scope.availableForAllRooms = function (presentationInRoom) {
            return presentationInRoom.room === 'ALL';
        };

        $scope.isDisabled = function (room) {
            if ($scope.agenda.visibleRooms.length > 1) {
                return false;
            }
            return $scope.agenda.visibleRooms.indexOf(room) != -1;
        };

        function findSlotBy(id) {
            return _.find($scope.agenda.schedule, function (item) {
                return item.slotId === id;
            });
        }

    });