'use strict';

angular.module('jelatynaSApp')
    .factory('voting', ['Vote', '$q', '$cookies', function (Vote, $q, $cookies) {
        function move(direction) {
            this.idx = this.idx + direction;
        }

        function getCurrent() {
            return this.items[this.idx];
        }

        function moveVote(direction) {
            var vote = getCurrent.call(this).vote + direction;
            if (vote < -1 || vote > 1) {
                vote = direction;
            }
            return vote;
        }

        return {
            items: [],
            idx: 0,
            get: function () {
                var owner = this;
                var deferred = $q.defer();
                var key = $cookies.key;
                var voteObj = Vote.get({key: key}, function () {
                    owner.idx = voteObj.voted;
                    owner.items = voteObj.presentations;
                    _.each(owner.items, function (item) {
                        if (item.vote == null) {
                            item.vote = 0;
                        }
                    });
                    $cookies.key = voteObj.key;
                    deferred.resolve(owner.items);
                });
                return deferred.promise;
            },
            isActive: function () {
                return this.items.length == 0 || this.idx < this.items.length;
            },
            next: function () {
                move.call(this, 1);
            },
            prev: function () {
                if (this.idx > 0) {
                    move.call(this, -1);
                    return true;
                } else {
                    return false;
                }
            },
            isCurrent: function (idx) {
                return this.idx === idx;
            },
            vote: function (vote) {
                getCurrent.call(this).vote = vote;
            },
            up: function () {
                this.vote(moveVote.call(this, 1));
            },
            down: function () {
                this.vote(moveVote.call(this, -1));
            },
            submit: function () {
                var current = getCurrent.call(this);
                var vote = new Vote();
                vote.key = $cookies.key || "";
                vote.rate = current.vote;
                vote.presentationId = current.presentation.id;
                var deferred = $q.defer();
                vote.$save(function () {
                    deferred.resolve();
                });
                return deferred.promise;
            },
            notStarted: function () {
                return $cookies.started == null;
            },
            start: function () {
                return $cookies.started = 1;
            }

        };
    }])
;
