'use strict';

angular.module('jelatynaSApp')
    .factory('Pages', ["$resource","api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/pages/:name', {name: '@name'}, {});
    }]);
