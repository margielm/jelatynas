'use strict';

angular.module('jelatynaSApp')
    .factory('News', ["$resource","api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/news/:page/:pageSize', {'page': 0, 'pageSize': 5}, {});
    }
    ]);
