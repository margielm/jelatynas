'use strict';

angular.module('jelatynaSApp')
    .factory('Registration', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/register');
    }]);