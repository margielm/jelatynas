'use strict';

angular.module('jelatynaSApp')
    .factory('Agenda', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/agenda');
    }]);
