'use strict';

angular.module('jelatynaSApp')
    .factory('Speaker', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer+'/speakers');
    }]);
